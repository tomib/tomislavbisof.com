const colors = {
  primary: '#2a2a31',
  secondary: '#fff',
  base: '#1b1b1f',
  levelOne: '#28282d',
  levelTwo: '#37353c',
  accent: '#ff8800',
};

export default colors;
