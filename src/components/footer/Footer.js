import React from 'react';

import { FooterStyled } from './styled';

export default function Footer() {
  return (
    <FooterStyled>
      © {new Date().getFullYear()}, Built with
      {` `}
      <a href="https://www.gatsbyjs.org">Gatsby</a>
    </FooterStyled>
  );
}
