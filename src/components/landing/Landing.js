import React, { useState, useEffect, useRef } from 'react';
import { navigate } from 'gatsby';
import { TimelineLite, Power1, Power2, CSSPlugin } from 'gsap/TweenMax';

import {
  LandingStyled,
  Entry,
  Logo,
  Greeting,
  CardOne,
  CardTwo,
  Button,
} from './styled';

import ImageLogo from '../../assets/logo.svg';

export default function Landing() {
  const [exiting, setExiting] = useState(false);
  const [mounted, setMounted] = useState(false);

  let elementEntry = useRef(null);
  let elementLogo = useRef(null);
  let elementGreeting = useRef(null);
  let elementButton = useRef(null);
  let elementCardOne = useRef(null);
  let elementCardTwo = useRef(null);

  CSSPlugin.defaultForce3D = true;

  useEffect(() => {
    let tlEnter = new TimelineLite();

    tlEnter.from(
      elementEntry,
      1.2,
      {
        scale: 1.5,
        rotation: -15,
        autoAlpha: 0,
        ease: Power1.easeInOut,
      },
      0
    );
    tlEnter.from(
      elementLogo,
      1,
      {
        y: 30,
        autoAlpha: 0,
        ease: Power1.easeInOut,
      },
      1.3
    );
    tlEnter.from(
      elementGreeting,
      1.2,
      {
        autoAlpha: 0,
        ease: Power1.easeInOut,
      },
      2.4
    );
    tlEnter.from(
      elementCardOne,
      1.4,
      {
        x: 600,
        y: 150,
        autoAlpha: 0,
        ease: Power1.easeInOut,
      },
      3.2
    );
    tlEnter.from(
      elementCardTwo,
      1.4,
      {
        x: 600,
        y: 150,
        autoAlpha: 0,
        ease: Power1.easeInOut,
      },
      3.4
    );
    tlEnter.from(
      elementButton,
      0.6,
      {
        autoAlpha: 0,
        ease: Power1.easeInOut,
      },
      4.8
    );

    setMounted(true);

    tlEnter.play();

    return () => {
      tlEnter.pause(0);
    };
  }, []);

  function onExit() {
    setExiting(true);

    let tlExit = new TimelineLite();

    tlExit.to(
      elementCardOne,
      1.4,
      {
        x: 600,
        y: 150,
        autoAlpha: 0,
        ease: Power2.easeInOut,
      },
      0
    );
    tlExit.to(
      elementCardTwo,
      1.4,
      {
        x: 600,
        y: 150,
        autoAlpha: 0,
        ease: Power2.easeInOut,
      },
      0.2
    );
    tlExit.to(
      elementEntry,
      0.8,
      {
        scale: 0.6,
        autoAlpha: 0,
        ease: Power2.easeInOut,
      },
      0.8
    );

    tlExit.play();

    setTimeout(() => {
      navigate('/page-2/');
    }, 2600);
  }

  return (
    <LandingStyled mounted={mounted}>
      <Entry
        ref={el => {
          elementEntry = el;
        }}
      >
        <Logo
          src={ImageLogo}
          alt=""
          ref={el => {
            elementLogo = el;
          }}
        />
        <Greeting
          ref={el => {
            elementGreeting = el;
          }}
        >
          Hello there.
        </Greeting>
        <Button
          variant="primary"
          onClick={onExit}
          disabled={exiting}
          ref={el => {
            elementButton = el;
          }}
        >
          Have a look inside
        </Button>
      </Entry>
      <CardOne
        viewBox="0 0 100 100"
        ref={el => {
          elementCardOne = el;
        }}
      >
        <polygon points="100,0 100,100 0,100" />
      </CardOne>
      <CardTwo
        viewBox="0 0 100 60"
        ref={el => {
          elementCardTwo = el;
        }}
      >
        <polygon points="100,0 100,70 0,70" />
      </CardTwo>
    </LandingStyled>
  );
}
