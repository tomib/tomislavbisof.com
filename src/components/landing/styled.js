import styled from 'styled-components';

import colors from '../../constants/colors';

import { ButtonStyle } from '../../components/shared/button/styled';

export const LandingStyled = styled.div`
  visibility: ${props => (props.mounted ? 'visible' : 'hidden')};
`;

export const Entry = styled.main`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  position: fixed;
  top: 50%;
  left: 50%;
  z-index: 4;

  width: 960px;
  height: 640px;

  background: ${colors.primary};

  transform: translate(-50%, -50%);

  box-shadow: 4px 4px 14px 0px rgba(0, 0, 0, 0.4);
`;

export const Logo = styled.img`
  display: block;

  width: 320px;

  margin-bottom: 40px;
`;

export const Greeting = styled.p`
  margin-bottom: 20px;

  color: ${colors.accent};

  font-size: 32px;
  font-weight: 300;
`;

export const CardOne = styled.svg`
  position: fixed;
  bottom: 0;
  left: -10%;
  z-index: 1;

  width: 110%;

  fill: ${colors.levelOne};
  filter: drop-shadow(-16px 8px 8px rgba(0, 0, 0, 0.6));
`;

export const CardTwo = styled.svg`
  position: fixed;
  bottom: 0;
  left: 10%;
  z-index: 2;

  width: 90%;

  fill: ${colors.levelTwo};
  filter: drop-shadow(-16px 8px 8px rgba(0, 0, 0, 0.6));
`;

export const Button = styled.button`
  ${ButtonStyle}
`;
