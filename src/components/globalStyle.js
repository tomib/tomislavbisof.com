import { createGlobalStyle } from 'styled-components';
import colors from '../constants/colors';

const GlobalStyle = createGlobalStyle`
  @import url('https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700');
  @import url('https://fonts.googleapis.com/css?family=Montserrat:300,500,800&subset=latin-ext');

  html, body, h1, h2, p, ul, li {
    margin: 0;
    padding: 0;
  }

  * {
    box-sizing: border-box;
  }

  body {
    background: ${colors.base};
    color: ${colors.secondary};

    font-family: 'Roboto Condensed', sans-serif;
  }

  h1, h2, h3 {
    font-family: 'Montserrat', sans-serif;
    font-weight: 800;
  }

  button {
    font-family: 'Roboto Condensed', sans-serif;
  }
`;

export default GlobalStyle;
