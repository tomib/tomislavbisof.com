import styled from 'styled-components';

import colors from '../../constants/colors';

export const HeaderStyled = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-between;

  width: 100%;
  max-width: 960px;

  margin: 0 auto;
  padding: 20px 0;
`;

export const Text = styled.div``;

export const Title = styled.h1`
  color: ${colors.secondary};

  font-size: 34px;
  font-weight: 800;
  line-height: 1.5;
`;

export const Description = styled.h2`
  color: ${colors.secondary};

  font-size: 18px;
  font-weight: 300;
`;

export const Logo = styled.img`
  width: 100px;
`;
