import React from 'react';

import { HeaderStyled, Text, Title, Description, Logo } from './styled';

import ImageLogo from '../../assets/logo.svg';

export default function Header() {
  return (
    <HeaderStyled>
      <Text>
        <Title>Tomislav Bišof</Title>
        <Description>Senior Frontend Web Developer</Description>
      </Text>
      <Logo src={ImageLogo} alt="Logo" />
    </HeaderStyled>
  );
}
