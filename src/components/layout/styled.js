import styled from 'styled-components';

import ImageBackground from '../../assets/background.jpg';

export const LayoutStyled = styled.div``;

export const Background = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: -1;

  background: url(${ImageBackground}) no-repeat center center fixed;
  background-size: cover;
`;

export const Content = styled.main``;
