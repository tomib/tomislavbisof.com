/**
 * Layout component that queries for data
 * with Gatsby's StaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/static-query/
 */

import React from 'react';
import PropTypes from 'prop-types';
import { StaticQuery, graphql } from 'gatsby';

import GlobalStyle from '../globalStyle';
import Header from '../header/Header';
import { LayoutStyled, Content } from './styled';

const Layout = ({ withHeader, children }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => (
      <LayoutStyled>
        <GlobalStyle />
        {withHeader && <Header siteTitle={data.site.siteMetadata.title} />}
        <Content>{children}</Content>
      </LayoutStyled>
    )}
  />
);

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
