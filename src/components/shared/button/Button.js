import React from 'react';

import { ButtonStyled } from './styled';

export default function Button(props) {
  return <ButtonStyled {...props}>{props.children}</ButtonStyled>;
}
