import styled, { css } from 'styled-components';

import colors from '../../../constants/colors';

export const ButtonStyle = css`
  padding: 10px 20px;

  background: transparent;
  color: ${colors.secondary};

  font-size: 36px;
  font-weight: 700;
  text-transform: uppercase;

  border: 2px solid ${colors.accent};
  border-radius: 3px;
  outline: 0;

  box-shadow: none;

  cursor: pointer;

  transition: 0.3s background ease-in-out, 0.3s color ease-in-out;

  &:hover {
    background: ${colors.accent};
    color: ${colors.secondary};
  }
`;

export const ButtonStyled = styled.button`
  ${ButtonStyle}
`;
