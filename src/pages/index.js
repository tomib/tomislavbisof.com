import React from 'react';

import Layout from '../components/layout/Layout';
import SEO from '../components/seo/Seo';
import Landing from '../components/landing/Landing';

export default function IndexPage() {
  return (
    <Layout>
      <SEO
        title="Home"
        keywords={[
          `tomislav`,
          `bisof`,
          `frontend`,
          `react`,
          `the hague`,
          `den haag`,
        ]}
      />
      <Landing />
    </Layout>
  );
}
