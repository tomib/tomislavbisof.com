import React from 'react';
import { Link } from 'gatsby';

import Layout from '../components/layout/Layout';
import SEO from '../components/seo/Seo';

export default function About() {
  return (
    <Layout>
      <SEO
        title="Home"
        keywords={[
          `tomislav`,
          `bisof`,
          `frontend`,
          `react`,
          `the hague`,
          `den haag`,
        ]}
      />
      About
      <Link to="/page-2/">Go to page 2</Link>
    </Layout>
  );
}
